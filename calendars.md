# Calendars

1. Ensure the person has a [May First Movement Technology](https://mayfirst.org/ ) user account.
2. Log into NextCloud at [share.mayfirst.org](https://share.mayfirst.org/ ) and go to [Calendar](https://share.mayfirst.org/apps/calendar/ ).
3. Create your own business calendar to be shared with teammates.
4. Share the newly created calendar with the Agaric team by clicking "Share" and then typing in the usernames of each member (do not trust the autocomplete, you will likely have to type the full username before the account shows up).
5. Ask to be added to the Agaric shared calendar.

## Norms and customs

  * Do not post anything to agaric-shared that is not definitely for everyone.
  * Mute notifications on calendars other than your own and agaric-shared.


## Integrating NextCloud calendars with Thunderbird

### Subscribe to a NextCloud calendar 

0. In NextCloud web calendar, press the three dots (**&middot;&middot;&middot;**) next to the calendar (your own or a colleagues) which you'd like to have in Thunderbird and press **Copy private link**
  ![Pop-up with the appearance of a dropdown menu next to Clayton's smiling face, with the top item being Edit name and a clipboard icon with Copy private link selected.](images/getting-clayton-calendar.png)
1. In Thunderbird calendar view, right-click in the empty space below your calendar and selecting **New Calendar...**
  ![Dropdown list starting with 'Show All Calendars' with 'New Calendar...' selected.](images/add-new-calendar.png)
2. Choose the **On the Network** option and press the **Next** button
  ![A dialog to 'Create a new calendar' with two radio button options and the second, 'On the Network' selected.](images/new-calendar-on-network.png)
3. Choose the **CalDAV** option, paste the private link from the first step in the **Location** text field (leave the username blank) and press the **Next** button
  ![A dialog to 'Create a new calendar' showing four radio buttons, the second one, 'CalDAV', is selected, and a URL is present in a text field labeled 'Location'.](images/thunderbird-add-nextcloud-calendar.png)
4. Give your calendar a name, sticking to the name used in NextCloud to the extent practical, and save the dialog, and you're done!
5. Ensure your calendar is associated with the e-mail address with which you want to receive invites.

```{note}
Members of Agaric can share their private calendar links to the [internal Agaric wiki](https://gitlab.com/agaric/internal/-/wikis/calendars ).
```
