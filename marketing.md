# Marketing

```{admonition} See also

[Agaric's content style guide](content-style-guide)
```

## Content


### Case studies

Proposals/contracts should include a clause permitting use of the project in case studies. [Case studies](https://agaric.coop/work) are their own, visually distinct content type; blog posts can then be tagged to them. Case studies should also be [posted to Drupal.org](https://www.drupal.org/case-studies/community).


### Blog posts

The most frequently updated section of the website are [blog posts](https://agaric.coop/blog). We collectively commit to have at least one post a week to our blog; that means each of us should produce at least one blog post a month.

All blog posts of interest to the Drupal community should be tagged to go to the [Drupal Planet aggregator](https://drupal.org/planet).


## [Agaric.coop](https://agaric.coop)

The flagship of our marketing efforts, where we control both content and presentation.

All content of any substance must also be here, not only places where we do not have full control. 

## Channels

Primary online marketing channels are our website and other communication over open protocols, such as Secondary online marketing includes places not fully in our control, such as Facebook, Twitter, and LinkedIn.

### Newsletter

14 years in, still haven't sent one.  But that needs to change.


### Drupal.org

Drupal marketplace used to be sorted by issue credits (which can include testing/review and an array of community contributions as well as code commits) in the past 90 days; at one time just 20 from all at Agaric will get us onto the front page.

The sorting has since changed to a secret algorithm that heavily favors monetary donations to the Drupal Association.  Credits without accounting for organization size (or at least having a filter, so people who prefer small and medium businesses can find them) already discriminated against small businesses.  With the pay-to-play changes it outright puts high ranking out of reach, especially for companies more committed to the community than to high income.


## Events

In addition to sponsoring, this can include more creative approaches such as offline entertaining educational material. A note on this, if there is a poster it should link to a page on the web site with consistent imagery and text.

Instead of sponsoring some events, we can both help the community more and get much more attention by sponsoring a person to get to the event— inviting nominations and applications ahead of time.


## Social media

Our current social media presences:

* Mastodon - <https://social.coop/@agaric>
* LinkedIn - <https://www.linkedin.com/company/agaric/>
* Twitter - <https://twitter.com/agaric>
* Facebook - <https://www.facebook.com/agaric.collective>
* Meetup - <http://meetup.com/agaric>
* Peertube - Apparently [we never decided (internal link)](https://gitlab.com/agaric/internal/-/issues/155) .. well there is <https://peertube.video/accounts/agaric@social.coop/video-channels> but nothing there.
* YouTube?

Content for Twitter in particular (aside from announcing and discussing our own blog posts) can include events we are organizing (global training days in Nicaragua), places we will be (cons and camps), links to other people's blog posts which we find useful. Stefan prefers posts by Agaric on any social network have some real connection to us.
