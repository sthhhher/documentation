# Project management with GitLab

Need to polish and publish our internal docs:

* https://gitlab.com/agaric/internal/wikis/Project-Management
* https://gitlab.com/agaric/internal/wikis/estimates
* https://gitlab.com/agaric/internal/wikis/version%20control

... and come to think of it, reorganize GitLab-specific notes as a subset of project management approach.



### Disable autoclosing issues from commits and merge requests

Under Repository Settings (Settings » Repository, for instance [`gitlab.com/agaric/sites/agaric-com/-/settings/repository`](https://gitlab.com/agaric/sites/agaric-com/-/settings/repository)) expand the **Default Branch** section and uncheck **Auto-close referenced issues on default branch**.

```{note}
See [GitLab help: Disabling automatic issue closing](https://gitlab.com/help/user/project/issues/managing_issues.md#disabling-automatic-issue-closing)
```

