# Monday Checkin

The Monday checkin ensures everyone is on the same page starting the week.  It is:

  * No longer than one hour, maximum.
  * Has a facilitator.
  * Has a designated notetaker, with everyone encouraged to participate.
  * Tasks should be [added to GitLab](https://gitlab.com/agaric/internal/-/boards/) as the meeting takes place.

Here is a template that can be pasted into a text pad (ideally markdown-aware).

```md
# 2021 Juluary 17th – Monday Checkin

## Checkins

*Quick updates on personal headspace, and work updates from today or things from weekend *

  * Mauricio
  * Chris
  * Micky
  * Sanjay
  * Ben

## Leads, or important projects


## Main focus for the week

*Attempt to specify just ONE important task that will be the focus for completion this week.*

  * Mauricio
  * Chris
  * Micky
  * Sanjay
  * Ben

## Blockers

  * Mauricio
  * Chris
  * Micky
  * Sanjay
  * Ben

## Task allocation

*List tasks throughout planning session, by end each task should have a person assigned to it.*
```
