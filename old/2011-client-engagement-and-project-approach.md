# Agaric Client Engagement and Project Approach 

***[Note: This document is from 2011 and does not necessarily reflect Agaric's current approach.]***

*The following proposal provides [the client] with an overview of Agaric's project approach and client engagement. *

Agaric's process is streamlined and tailored to accord efficiently with the specifications of Drupal, an open-source content management system (CMS).  Through the process, Agaric adheres to high standards of collaboration, communication, active client participation, and transparency.  Agaric combines the best practices and standards of web—in particular Drupal development, with a flexible approach that facilitates in designing for the user's experience and ensuring timely delivery of the project.  Agaric is committed not only to web standards and accessibility but also to following collaborative development best practices.  We use Git for all of our repository work and export as much Drupal configuration to code as practical.  We work based on an agile development methodology: iterative and incremental development, where requirements and solutions evolve through collaboration with product owners, development staff, and all team members. 

## The Agaric Approach

Our foundation for all our client work begins by answering two questions that define the project goals and needs, taking into account both the business and end-user: 

What do we want to get out of this site? *(The business requirements)*  
What do our users want to get out of it? 

Once we—client and Agaric—have defined and collectively understand the strategy, we focus on the scope of the site. Scope is where we define what content and functionality the site will offer to its users and administrators. The decisions on what we implement are always driven by our client's goals and needs. Armed with a clear picture of what needs to be included in the project, we can define the structure—interaction design and information architecture—and skeleton—interface, navigation, and information design—prototyping in the medium as much as possible. 

The visual design is the most concrete and final aspect of the project. If any visual work is required, the final designs are completed only after the skeleton and structural requirements are settled. Visual language exploration can be included in the strategy phase. At Agaric, we tap into the theories of emotional design to create responsive and engaging design that keeps users wanting to come back. 

The Style Guide is a key design deliverable. It ensures that we design and account for all Drupal elements we have instead of fitting the technology to the design, another costly pitfall of development that our process seeks to avoid. 

## Great! So, How Do We Start? 

Agaric projects start with a fixed $15,000, one-week strategy phase.  In the event that more time is required, Agaric will inform the client prior to the commencing the strategy phase.  Within the week, Agaric and the client both commit to actively working together through meetings, shadowing work processes and other engagement mediums.  Onsite engagement may take place during the first two or three days.  

At the end, Agaric will prepare and deliver the Web Site Strategic Analysis and Technical Recommendation, as well as any other assets prepared during that time—personas, visual language exploration. The Web Site Strategic Analysis and Technical document includes our recommendations for approach and development—the Project Plan—as well as cost estimates. 

The Project Plan also presents additional features and their benefits to the initially defined overall project goals. Upon agreement on the initial feature set, Agaric moves forward to the build phase sprints. We prepare the necessary user stories and cases for tasks. We closely follow iterative development methodology for product management. At all times, we ensure that our clients can monitor progress on staging servers and case burn rate. 

Iterative development ensures that we QA throughout the project, however, dedicated time for QA is required before each push live.
