# Worker Owner Meeting

Copy-pastable template:

```
# 2020 November 24

## Check-ins (i.e. how are you, generally?)

* Ben 
* Chris
* David 
* Mauricio 
* Micky 

## General Updates (work items)

* Ben 
* Chris
* David 
* Mauricio 
* Micky 

## Financial

Checking: 
Savings: 
Credit: 
https://pad.drutopia.org/p/private_financial-transaction-of-note

### Payable
### Receivable
### Transactions 


## Marketing
### Blog Posts

## Training

## Decisions to make

## Good News
* Ben 
* Chris
* David 
* Mauricio 
* Micky
```
