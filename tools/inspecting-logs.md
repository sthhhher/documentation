# Inspecting logs

ssh into a live or test server or your local development environment:

```bash
journalctl
```

```bash
sudo journalctl -t drupal
```

Much more you can do w/journalctl as well. For example, -f to follow (like tail -f) and --since to filter time:

```bash
sudo journalctl -t drupal --since "5 minutes ago" -f
```


