# Git Usage

## When NOT to manually resolve merge conflicts

When it's an automatically generated file!

For composer.json:

```
rm composer.lock
ddev composer update
git add composer.lock
git commit
```
