# Deploying Drutopia updates

## Prerequsites

Note that this is sort of 

```bash
sudo apt-get install ansible rsync php7.4 php7.4-gd php7.4-mysql php7.4-xml php7.4-curl php7.4-fpm php7.4-sqlite3 php7.4-cli
```

(Yeah that's more PHP than you need.)

The commands from:

https://getcomposer.org/download/

And then:

```bash
sudo mv composer.phar /usr/local/bin/composer
```

```bash
cd ~/Projects/drutopia-platform/drutopia_host/hosting_private
ahoy git-pull-all
```

## Bonus: Keep Drutopia builds with similar available modules

To try to keep various Drutopia-based distributions from diverging too much, at least insofar as available modules, even if they aren't installed, we can use the **meld** (`sudo apt-get install meld`) diff tool to compare and share when posssible.

```bash
meld ~/Projects/agaric/sites/crla/crla-org/composer.json ~/Projects/agaric/sites/geo/composer.json ~/Projects/drutopia-platform/build_source/composer.json ~/Projects/agaric/sites/agaric-com/composer.json
```

When these align in not needing special patches or versions, we can consider dropping a custom build in favor of collaborating on a single one.
