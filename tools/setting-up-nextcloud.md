# Setting up Nextcloud for a client

There are several non-intuitive steps to get Nextcloud set up and gotchas which clients have to know about.

It is not possible to add accounts or change passwords in May First's Nextcloud instance, [share.mayfirst.org](https://share.mayfirst.org/).  This must be done in May First's control panel.

Log into [May First's control panel](https://members.mayfirst.org/cp/) with the organization's May First account and create a new User Account to use exclusively for Nextcloud, for instance `exampleorg-nextcloud`, with a strong password.  It is not necessary to provide an e-mail address.

Also here, create user accounts for any people who will be using Nextcloud and do not already have May First accounts (either in the organization *or* anywhere else— May First accounts are universal across most tools provided by May First, including Nextcloud).

0. Log into [share.mayfirst.org](https://share.mayfirst.org/) with this new Nextcloud-only account.
1. Go to [Circles](https://share.mayfirst.org/apps/circles/)
2. Type a no-spaces version of the clients name (for instance, `exampleorg`) into the "Create a new circle" box at the top left, under the Nextcloud logo.
3. For *Select a circle type* choose "Create a new personal circle".

Ask the people with accounts on May First to use the same username and password to sign into Nextcloud at [share.mayfirst.org](https://share.mayfirst.org/) 

Add each of those people to the personal circle (use the same username as they have in the user account in the May First control panel, but they do need to sign into Nextcloud before you can add them).

Under [Files](https://share.mayfirst.org/apps/files/) on Nextcloud create a folder named after the organization (for instance `exampleorg`).  Share that folder with the personal circle you previously created.

Now every folder you create *within* that folder will be available to everyone in the organization (the people you added to the circle).  And even if a person has multiple affiliations on May First's Nextcloud, the files related to the organization will be clearly namespaced.

```{admonition} Further reading

  * <https://support.mayfirst.org/wiki/nextcloud>
```

## Rationale

We create an organizaton account so that it's clear people are being invited to organization resources and there is no problem if an individual leaves an organization.

Because the account will have the same password for Nextcloud and for everything else in May First, we create an account through the May First control panel that will be used only for Nextcloud.  This way credentials used, even rarely, for logging into Nextcloud are not also used to control web hosting, e-mail, and other 



## Original documentation to reconcile

When Agaric hosts a new client on MayFirst, the client gains access to the free software tools on the MayFirst platform.
Any user account in the MayFirst control panel can login to nextcloud with their MayFirst.org username and password.
NextCloud - https://share.mayfirst.org

Setup Client Access to NextCloud:
    
1. Login to NextCloud and create a client-nextcloud-admin user for the puprose of creating circles and organizing the nextcloud folders for the group.
2. Login to the Members Control Panel and create a user account for each person you will be sharing with. 
  2a. You can send them a password reset email (click "Password Reset") in the left hand side bar.
3. Create a secret circle in NextCloud using the circles icon on the toolbar
4. Send an email to the client with instructions to log into NextCloud at https://share.mayfirst.org with their MayFirst username and password.
5. After a client has logged in, you will be able to share folders and files.

Be sure everyone in the secret circle logs into NexCloud ​https://share.mayfirst.org/ so you can share folders with them. 
You cannot share a folder or calendar with someone who has not yet logged in. 



MayFirst suggestions for how groups can effectively use nextcloud to share. The steps are here: https://support.mayfirst.org/wiki/nextcloud#CanIcreategroupsofpeopletosharewith
