# Securely Transferring Files Via NextCloud

## Internally/Users with MayFirst

Clients with MayFirst hosting can be shared with directly in NextCloud, using their MayFirst login. [Unable to confirm this works - definitely works within Agaric members]

### Steps

1. Ensure the file you want to share is stored on NextCloud.
2. View the details of the file (from web UI select "..." drop down for file and select details).
3. On the sharing tab, look up the user's email address or MayFirst user name and select them.
4. From the drop down "..." next to the user, you can specify access level and expiration, etc.

# Externally

Use this method when a file needs to be shared with an individual outside the organization. 

This represents a suggested flow for securely sending a file, using email to communicate. A shared link is instead used to make the transfer, with steps to be followed to ensure unencrypted email snooping is highly unlikely to be an issue.

[Review existing shared files/links](https://share.mayfirst.org/apps/files/?dir=/&view=shareoverview)

## Steps

1. Ensure the file you want to share is stored on NextCloud.
2. View the details of the file (from web UI select "..." drop down for file and select details).
3. On the sharing tab, click share link.
4. Click the "..." drop down for the link, and add a password. A default will fill in for you - improve it, if warranted.
5. Optionally, you might also reduce the default expiration to just a day or so.
6. Copy the password for the link, and head to https://onetimesecret.com/ and create a secret there containing that password. Optionally, add a password for the secret retrieval (again: as warranted).
7. Send the one-time secret link to the recipient via email (along with the password for retrieiving it). _Do not yet send the shared URL._ Ask for a response confirming the password has been acquired. For example: >We will share a link to the file with you via a safe transfer service. In order to download the file, you will need a password. The password can be retrieved from {onetimepassword link}. Use the password {password} to see this secret. Once you have copied the password to your computer, let us know, and we can share the link to the file.
9. Once the recipient confirms receiving the password (meaning no one else can then access it via the onetimesecret link), send the share link to the file.