# Wednesday Checkin

The Monday checkin ensures everyone is on the same page starting the week.  It is:

  * No longer than one hour, maximum.
  * Tasks should be [added to GitLab](https://gitlab.com/agaric/internal/-/boards/) as the meeting takes place.

Here is a template that can be pasted into a text pad (ideally markdown-aware).

```md
# 2021 Juluary 19th – Wednesday Checkin

## Updates

  * Mauricio
  * Chris
  * Micky
  * David
  * Ben

## Blockers

  * Mauricio
  * Chris
  * Micky
  * David
  * Ben

## Task allocation

*List tasks throughout, by end each task should have a person assigned to it.*

https://gitlab.com/agaric/internal/-/boards/ 
```
