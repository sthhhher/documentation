# Weekly Rhythm

Agaric's weekly communication is structured like this:

 * [Monday check-in](monday-checkin)
 * Tuesday worker-owner meeting
 * [Wednesday check-in](wednesday-checkin)
 * [Friday review & planning](friday-review-and-planning)

We meet every day at 4 pm, except for Thursday which is meeting-free.

One hour is the maximum time for any meeting (including Friday review & planning and Monday checkin).

No (internal co-op-wide) meetings on Thursdays.

The worker-owner meeting happens weekly with an agenda in advance. If there are no topics put forward to discuss it is skipped.  There is no check-in (stand up) meeting on Tuesday.

