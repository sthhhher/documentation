# Worker-Owner Meeting

The worker-owner meeting should mostly be populated with agenda items.

It replaces the daily standup, but ideally we would separate daily items to the daily/weekly rythm pad and leave the worker-owner notes with only the higher-level subjects.  Switching pads during the meeting is too disruptive but maybe moving check-ins and work updates to the daily pad after the meeting would work.


```
# 2020 September 29


## Check-ins (i.e. how are you, generally?)

* Ben - 
* Chris - 
* David -
* Mauricio - 
* Micky - 


## General Updates (work items)

* Ben
* Chris
* David
* Mauricio overloaded, but managing to get word done.
* Micky


## Financial

Checking: 
Savings: 
Credit: 


## Marketing


## Training

```
