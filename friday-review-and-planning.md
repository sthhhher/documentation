# Friday Shipping Meeting

```md
# 2020 {Date}, Friday shipping

## Major Accomplishments/Hurdles Cleared?

## Updates

### Ben
* 

### Chris
* 

### David
* 

### Mauricio
* 

### Micky
* 

### Sanjay
*

## Metrics Review

1- low; 5-high

### Stress Level

* Ben - 
* Chris - 
* Mauricio - 
* Micky - 
* Sanjay

### Satisfaction Level

* Ben - 
* Chris -  
* Mauricio - 
* Micky - 
* Sanjay

### Hours Entered

* Ben - 
* Chris - 
* David - 
* Mauricio - 
* Micky -
* Sanjay 

## Availability

*Please note just any business hours or other necessities you may not be able to be present for next week.*

## Project updates

*What projects need our attention in the coming week?*

## Learning Round

* Neat tips & tricks, or nasty bugs squashed

## Thanks

* Ben - 
* Chris - 
* Mauricio - 
* Micky - 
* Sanjay
```
