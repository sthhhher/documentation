Shared technical notes
======================

It takes some attention to ensure no confidential information goes out, and these notes are meant to be taken without thinking.  Therefore, [Agaric's raw notes repository is not public](https://gitlab.com/agaric/raw-notes/).

Notes which are not marked draft, however, are automatically published to [agaric.gitlab.io/raw-notes](https://agaric.gitlab.io/raw-notes/).

Background
----------

In the early days of Agaric, [ben](http://agaric.com/mlncn) posted every error message, search query, and hare-brained musing to agaric.com.  Under editorial pressure from collective members this was exiled to [data.agaric.com](http://data.agaric.com/), built by worker-owner Kathleen Murtagh in the then-new Drupal 6.  Eventually ben internalized some sort of standards for publication, and the pace of posting dropped off— his incessant notetaking found a home in a local, unshared wiki-like notes application for GNU/Linux, Tomboy.

These notes were hard to share, however, so we've gone to a middle ground of taking notes locally in Git and publishing using GitLab Pages.

The hard part for making it anywhere near as easy as Tomboy, which auto-titled notes, was making it so that typing out a file name to create or save a note wasn't necessary.  The [Save as Heading package gave us an easy way to save files in Atom](https://agaric.gitlab.io/raw-notes/notes/2018-06-24savingfileseasilyinatom/), so we went with [Atom](https://atom.io/) despite its non-negligible resource usage with tons of notes.

Future
------

Make it easy to publish from the raw-notes repository to [data.agaric](http://data.agaric.com), such as by adding a 'publish: true' line of metadata.
