# Roles

## Rotating cooperative-wide roles

In the interest of distributing types of work equitably and skill-sharing, a number of tasks rotate monthly or somewhat randomly.

### Standup facilitator

### Monday planning facilitator

### Friday shipping facilitator

### Worker-owner meeting facilitator

### Timecop

* Checks to ensure people get their time in each day by the end of Monday planning, mid-week daily standup, and Friday shipping.  Works with people individually after the meeting to 

### Scribe

* Write one sentence about each person's main activity in a week, summarizing our activities in an accessible way in blog posts such as "The Week That Was: Agaric's March 30th to April 3rd"
* As our main storyteller for the month, think about ways to weave the work we're doing every week into a narrative; if we can't fit our work into a narrative we're probably straying from our strategic goals.


## Project-specific roles

### Lead

### Developer/Designer
