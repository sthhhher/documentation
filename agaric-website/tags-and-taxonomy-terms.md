# Tags and taxonomy terms

## Tags

Any content editor can create a tag, but that doesn't mean you should.  The purpose of tags

(ideally, if a taxonomy term is on only one piece of content, it won't even be shown (or at least won't be linked).

On the other hand

### Naming tags

Tags should not be capitalized unless part of the tag is a proper name (for example, "meetups" and "Boston Drupal meetup" are both correct).

Tags should be pluralized when not a typically unpluralized category.

For instance, tags for a post expounding on the meaning of fast food:

WRONG: "deep thought", "philosophies", "pizzas", "sandwich"  
CORRECT: "deep thoughts", "philosophy", "pizza", "sandwiches"

The reason, aside from the ontological, is that we display tags as an opportunity for people to see additional posts covering the same topics, like this:

> ### More about…
>
> Drupal 8, Drupal Planet, and code snippets

A singular "code snippet" in that list would look pretty strange.

