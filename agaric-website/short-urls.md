# Short URLs

Drupal 8's redirect module supports creating redirects to external URLs.  This means it can be used as your own custom URL shortener!

1.  Add a redirect at https://agaric.coop/admin/config/search/redirect/add
2.  Make the path for your redirect start with `r/` to avoid the possibility of a conflict with a future page on the site.
3.  If you get an error, try selecting a language to start.  You can edit the redirect to remove the language later.

For external redirects, we recommend 302 Found (moved temporarily) so that you can update it if conditions out of your control change.


You can edit your redirect by searching for it at https://agaric.coop/admin/config/search/redirect

