# Cooperative Principles

Cooperatives around the world generally operate according to the same core principles and values, [adopted by the International Co-operative Alliance in 1995](https://ica.coop/en/whats-co-op/co-operative-identity-values-principles ). Cooperatives trace the codification of these principles to a cooperative founded in Rochdale, England, in 1844.

1. Voluntary and Open Membership
2. Democratic Member Control
3. Members' Economic Participation
4. Autonomy and Independence
5. Education, Training and Information
6. Cooperation among Cooperatives
7. Concern for Community

The cooperative principles resonate with our [purpose](purpose) and underlie our [values](values).
