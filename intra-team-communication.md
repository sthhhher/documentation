# In-team communication

Tasks people need to do go in GitLab, but that is *not* how tasks should be communicated.  Whenever possible, anything important enough to be assigned is worth talking about in 'real-time' first, or concomitantly.  Everything needed to understand an issue should be captured in GitLab, but we want to avoid back-and-forth 'huh what?' and 'nuh-uh not me' (un-assign)

Discussion that takes place outside of GitLab needs to be summarized (or copy-pasted) into the relevant issue in the GitLab project.

Try to get important meetings, internal as well as external, on your [Agaric calendar](calendars).

## Zulip

Our primary tool for internal communication is now [Zulip](https://zulipchat.com/), free software group chat software available as [LibreSaaS](https://libresaas.org/).

[agaric.zulipchat.com](https://agaric.zulipchat.com) is our web interface.

[Downloading an application](https://zulipchat.com/apps/) is recommended.


### E-mailing messages to Zulip

When BCCing a Zulip stream on a message to someone, Use the default that can be found under stream settings (click the gear icon you can get to from the down caret):

```
project.785f16fc671a5d8c0f2d4fbb161f16b3.show-sender@streams.zulipchat.com
```

When forwarding an e-mail to a Zulip stream, swap out `.show-sender` for `.include-footer.include-quotes.prefer-html`

```
project.785f16fc671a5d8c0f2d4fbb161f16b3.include-footer.include-quotes.prefer-html@streams.zulipchat.com
```

```{info}
More about [sending a message to a Zulip stream by e-mail](https://zulipchat.com/help/message-a-stream-by-email)
```

Hover over a stream in the lefthand column to get to stream settings; there's also a link to a list of all streams that is easier: https://agaric.zulipchat.com/#streams/all

Stream settings pages are also key for getting the e-mail address for sending e-mail to streams, https://agaric.zulipchat.com/help/message-a-stream-by-email (their help is lightly customized to organization, so that "Your streams" link will work)


### Zulip <-> IRC bridge

This package has been installed using (sort-of) the instructions at https://agaric.zulipchat.com/integrations/doc/irc

The bot currently lives on irc.agaric.com under the zulip account with software at /srv/zulip.

To launch the bot, login as root, and then:
```
su - zulip # become user for bot for agaric
startzulip
ctrl+d # switch back to root
su - zulip-nichq # become user for bot for nichq
startzulip
```

This should start zulip in the background and you can now disconnect. If there are errant processes, use `ps aux|grep zulip` to locate PIDs and `kill {pid}` of each. Also, you can `kick bot_zulip` on IRC if you are an IRC operator, which should also kill an existing process.


## BBB - BigBlueButton video chat

Agaric uses BigBlueButton for our daily Stand Up meetings and for client meetings. We have an account with http://meet.coop and you can find our room is here: https://ca.meet.coop/b/aga-52x-qa8

## IRC


\#agaric

Agaric also maintains a channel for worker-owners only. NOTE: We have not been using IRC sine Zulip arrived, but some of us still hang out there and use it to connect with other developers that are not in Zulip.

Freenode has given us operator privileges for this channel.  To use it, we need to register:

```
/msg nickserv identify mlncn pa55w0rd
/chanserv op #agaric mlncn
```

Some of our best clients are also on IRC, as are our partners at [May First Movement Technology (#mayfirst on irc.indymedia.org)](https://support.mayfirst.org/wiki/faq/chat).

### IRC Bouncer ###

**This is currently not active. There is an admin account installed/configured, but not auto-starting, and currently not running. If any member wants to restore the service for themselves, feel free!***

In order to acquire a 1.7 edition of ZNC, the backports for stretch were added and utilized for this package.

ZNC 1.7 is an IRC bouncer listening on port 1025 of irc.agaric.com.

A web interface is available for management at: https://irc.agaric.com:1025 that enables module maintenance, etc.

The entire app runs under the local system user znc-admin (just run `znc` as the znc-admin user).

### Web-based IRC client: The Lounge

We can access our IRC (and any other IRC) through:

https://irc.agaric.com

#### The Lounge Management

[The Lounge is a self-hosted web IRC client](https://thelounge.github.io/) we're using to provide https://irc.agaric.com

Everyone in [SSH_public_keys]() as of 2017 August has access to the Digital Ocean droplet (1cpu/512mb) hosting it:

`ssh root@irc.agaric.com`

##### Upgrading

TheLounge is installed manually via dpkg as there is no current apt source for it.

To upgrade via dpkg:
- Go to https://github.com/thelounge/thelounge/releases/ and select your release
- Copy the link to the deb file at the bottom of the releases page.
- `ssh root@irc.agaric.com` and run `wget -L -o thelounge.deb <copied link>`
- Install the new package with `dpkg -i thelounge.deb`
- The service should restart, but if it does not, `systemctl restart thelounge`

There is also an option to use `thelounge update` but it is unclear if this is preferable to using dpkg.

##### Configuration

TheLounge is bound to the loopback address at port 9000 and reverse proxied via NGINX. The configuration and user files are located at `/srv/lounge/`. The configuration is pointed at via an Environment variable (the variable can be inspected/changed via `systemctl edit thelounge`).

##### TLS(/SSL)

NGINX will bounce HTTP connections to HTTPS and handle encryption via reverse proxy. Certbot is installed and should be handling automatic renewals with reload of NGINX as needed.

##### Management

Note that because the ENV var is depended upon for our configuration, and it runs as thelounge user. In order to facilitate simpler management, an alias is defined for thelounge under the root account.

This is configured for you (as root):
```
alias thelounge='sudo -u thelounge THELOUNGE_HOME=/srv/lounge thelounge'
```

List users:

```
thelounge list
```

Addin a user:

```
thelounge add username
```

Where `username` is the IRC nick for the user you are adding.

reset pwd:

```
thelounge reset username
```

Additional management commands can be found in [TheLounge documentation](https://thelounge.chat/docs/users).

### IRC Bot (Limnoria aka Supybot)

#### History

Stefan Freudenberg selected and installed Supybot as Agaric's general-purpose information bot (in particular expanding issue numbers to issue titles) circa 2010. Early in 2018 Chris Thompson upgraded Supybot — "a robust (it doesn't crash), user friendly (it's easy to configure) and programmer friendly (plugins are extremely easy to write) Python IRC bot. It aims to be an adequate replacement for most existing IRC bots. It includes a very flexible and powerful ACL system for controlling access to commands, as well as more than 50 builtin plugins providing around 400 actual commands" — to Limnoria — a project which continues development of Supybot.

It is installed on Simone.

#### Common commands

@later tell username A message

(until we override @tell to use the far more useful 'later' flavor)

#### Official documentation

* https://github.com/ProgVal/Limnoria
* http://doc.supybot.aperio.fr/en/latest/index.html

## Internal notes

Agarics can get more detail [on communication channels in the wiki](https://gitlab.com/agaric/internal/wikis/Communication-Channels).
